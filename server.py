from flask import Flask
import json

#app = Flask(__name__, static_url_path= '/static')
app = Flask(__name__)

bookings = json.load(open("all-bookings.json","r"))

@app.route('/available/<flight>')
def available(flight):
  return json.dumps(bookings[flight])

@app.after_request
def default_headers(response):
    response.headers["Content-Type"] = "application/json"
    response.headers["Access-Control-Allow-Origin"] = "*"
    return response

if __name__=='__main__':
  app.run(host='0.0.0.0',port=5036,debug=True)

